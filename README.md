# PM1
Project Management Information System

## Explanation
Several features :
- Ability to create new project and add personnel members to the project.
- Add and edit subtasks.
- Show overall project’s progress based on the subtasks done and on-progress ratio.
- Change subtasks status between To-Do, In-Progress, Finished, and Revision.
- Upload & download files.

## Installation
1. Download then extract the program.
2. `cd` inside extracted folder 
3. Run `composer install` to install dependencies.
4. Run `cp .env.example .env` then change the relevant .env parameters, e.g. DB_HOST, DB_PORT, etc.
5. Run `php artisan key:generate`
6. Run `php artisan migrate` to migrate tables to database.
7. Run `php artisan db:seed` to fill the tables with dummy data - this step is optional.

- Login : all password : password
- administrator : superadmin@admin.com 
- kepala divisi : head@admin.com
- pegawai1 : dodi@mail.com

