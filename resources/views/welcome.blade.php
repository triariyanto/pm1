<!DOCTYPE html>
<html>
 <head>
  <title>Work Management</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
   .box{
    justify-content: center;
    align-items: center;
    width:30%;
    margin-top:50px;
    border:1px solid #ccc;
   }
  </style>
 </head>
 <body>
  <br />

    <div align="center">
        <img src="{{ asset('bootstrap/img/logo.png') }}" style="display: inline-block; " href="{{ route('home') }}">
    </div>
  <div class="container box">
    
    <h3 align="center">Work Management</h3><br />

   @if(isset(Auth::user()->email))
    <script>window.location="/main/successlogin";</script>
   @endif

   @if ($message = Session::get('error'))
   <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
   </div>
   @endif

   @if (count($errors) > 0)
    <div class="alert alert-danger">
     <ul>
     @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
     @endforeach
     </ul>
    </div>
   @endif

   <form method="post" action="{{ route('login') }}">
    {{ csrf_field() }}
    <div class="form-group">
     <label>Email</label>
     <input type="email" id="email" name="email" class="form-control" />
    </div>
    <div class="form-group">
     <label>Password</label>
     <input type="password" id="password" name="password" class="form-control" />
    </div>
    <div class="form-group text-right">
     <input type="submit" name="login" class="btn btn-primary" value="Login" />
    </div>
   </form>
  </div>
 </body>
</html>