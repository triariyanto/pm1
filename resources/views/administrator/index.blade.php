@extends('layouts.administrator')

@section('title')
    Home
@endsection

@section('navbar')
    <li><a href="{{ route('user.manajemen') }}">User Pengguna</a></li>
    <li><a href="{{ route('kegiatan.index') }}">Task</a></li>
    @endsection

@section('content')
    <div class="row">
        {{-- <div style="text-align: center">
            <div class="title" style="font-size: 32px">
                Selamat Datang, {{ \Illuminate\Support\Facades\Auth::user()->name }}
                <br><br>
            </div>
        </div> --}}
        <br>
        <div class="form-group col-lg-6">
            <a href="{{ route('user.manajemen') }}" class="btn btn-lg btn-default custom-button-user pull-right"> User Pengguna</a><br>
        </div>
        <div class="form-group col-lg-6">
            <a href="{{ route('kegiatan.index') }}" class="btn btn-lg btn-default custom-button-project">Task</a>
        </div>

        <div class="form-group col-lg-6">
            <a href="{{ route('user.show', Auth::user()->id) }}" class="btn btn-lg btn-default custom-button-account pull-right">Account</a><br>
        </div>
        <div class="form-group col-lg-6">
            <a href="{{ route('logout') }}" class="btn btn-lg btn-default custom-button-logout"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </div>

@endsection
