<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $pegawai = new \App\Models\User();
        $pegawai->id = '1';
        $pegawai->name = 'superadmin';
        $pegawai->email = 'superadmin@admin.com';
        $pegawai->password = bcrypt('password');
        $pegawai->alamat = $faker->address;
        $pegawai->telepon = $faker->phoneNumber;
        $pegawai->note = 'administrator';
        $pegawai->jabatan_id = '1';
        $pegawai->save();

        $pegawai = new \App\Models\User();
        $pegawai->name = 'Giant';
        $pegawai->email = 'head@admin.com';
        $pegawai->password = bcrypt('password');
        $pegawai->alamat = $faker->address;
        $pegawai->telepon = $faker->phoneNumber;
        $pegawai->note = 'kepala divisi';
        $pegawai->jabatan_id = '2';
        $pegawai->save();

        $pegawai = new \App\Models\User();
        $pegawai->name = 'Dodi';
        $pegawai->email = 'dodi@mail.com';
        $pegawai->password = bcrypt('password');
        $pegawai->alamat = $faker->address;
        $pegawai->telepon = $faker->phoneNumber;
        $pegawai->note = 'pegawai';
        $pegawai->jabatan_id = '3';
        $pegawai->save();

        foreach (range(1, 10) as $index) {
            $pegawai = new \App\Models\User();
            $pegawai->name = $faker->name();
            $pegawai->email = $faker->companyEmail;
            $pegawai->password = bcrypt('password');
            $pegawai->alamat = $faker->address;
            $pegawai->telepon = $faker->phoneNumber;
            $pegawai->note = 'pegawai';
            $pegawai->jabatan_id = '3';
            $pegawai->save();
        }
    }

}
